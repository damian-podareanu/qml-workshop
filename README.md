# qml-workshop

These are the materials presented at the workshop that took place on the 13th of December

## PennyLane tutorial
* https://pennylane.ai/qml/
* ```tutorial.pptx``` - PennyLane slides
* ```quantum_neural_net.py``` - PennyLane function fitting
* ```tutorial_qubit_rotation.ipynb``` - PennyLane qubit rotation tutorial 
* ```data/``` - Data folder for the PennyLane handson
 